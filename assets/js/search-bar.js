function openSearch() {
  const searchBar = document.getElementById("search-container");
  searchBar.classList.add("open");
}

function closeSearch() {
  const searchBar = document.getElementById("search-container");
  searchBar.classList.remove("open");
}
